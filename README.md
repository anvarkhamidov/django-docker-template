# Django template in docker containers for development and production.

## Quickstart


#### Project files
- `.env.dev`, `docker-compose.yml` for development. 
- `.env.prod`, `.env.prod.db`, `docker-compose.prod.yml` for production.

___

#### Requirements
- Docker installed.

___

### Development
- Rename environment file `.env.dev-sample` to `.env.dev`.

**Build and run:**
```shell script 
$ docker-compose up -d --build
```
_Open up http://localhost:8000/ to test._

___

### Production
- Rename environment files `.env.prod-sample` and `.env.prod.db-sample` to `.env.dev` and `.env.prod.db` respectively.

**Build and run:**
```shell script 
$ docker-compose -f docker-compose.yml up -d --build
```
**Run `migrate` command:**
```shell script
$ docker-compose -f docker-compose.prod.yml exec web python manage.py migrate --noinput
```
**Run `collectstatic` command:**
```shell script
$ docker-compose -f docker-compose.prod.yml exec web python manage.py collectstatic --no-input
```

_Open up http://localhost:1337/ to test._
